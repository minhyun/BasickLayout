package com.thomas.study.basiclayout2.Fragment2Recyclerview;

import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thomas.study.basiclayout2.R;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomas on 2017-08-06.
 */

public class Tadapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private OnItemClickListener mItemClickListener;

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private ArrayList<Tdata> itemList;

    private OnLoadMoreListener onLoadMoreListener;
    private  LinearLayoutManager mLinearLayoutManager;

    private boolean isMoreLoading = false;
    private int visibleThreshold = 1;
    int firstVisibleItem, visibleItemCount, totalItemCount;

    //more 인터페이스
    public interface OnLoadMoreListener {
        void onLoadMore();
    }


    //클릭 인터페이스
    public interface OnItemClickListener{
        public void onItemClicked(RecyclerView.ViewHolder holder, View view, Tdata data, int positiion);
    }

    //생성자
    public Tadapter(OnLoadMoreListener listener) {
        onLoadMoreListener=listener;
        itemList=new ArrayList<>();
    }
    //리사이클뷰 레이아웃매니져 가져오기
    public void setLinearLayoutManager(LinearLayoutManager linearLayoutManager){
        this.mLinearLayoutManager =linearLayoutManager;
    }
    //리사이클뷰에 스크롤 리스너 달기
    public void setRecyclerView(RecyclerView mView){
        mView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLinearLayoutManager.getItemCount();
                firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
                if (!isMoreLoading && (totalItemCount - visibleItemCount)<= (firstVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isMoreLoading = true;
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    //클릭 리스너
    public void setOnItemClickListener(OnItemClickListener listener){
        mItemClickListener =listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {
            return new Tholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_fragment1, parent, false));
        } else {
            return new TloadHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loadmore, parent, false));
        }
    }
    //데아ㅣ터넣기
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        //Tholder tholder = (Tholder) holder;
        if(holder instanceof Tholder) {
            ((Tholder)holder).setItemData(itemList.get(position));
            ((Tholder)holder).setOnItemClickListener(mItemClickListener);
        }

    }

    //데이터 체인지
    public void addAll(List<Tdata> lst){
        itemList.clear();
        itemList.addAll(lst);
        notifyDataSetChanged();
    }
    //더보기시 데이터설정
    public void addItemMore(List<Tdata> lst){
        itemList.addAll(lst);
        notifyItemRangeChanged(0,itemList.size());
    }

    //more 리스너
    public void setMoreLoading(boolean isMoreLoading) {
        this.isMoreLoading=isMoreLoading;
    }

    //데이터 갯수
    @Override
    public int getItemCount() {
        return itemList.size();
    }
    //프로그래스 설정
    public void setProgressMore(final boolean isProgress) {
        if (isProgress) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    itemList.add(null);
                    notifyItemInserted(itemList.size() - 1);
                }
            });
        } else {
            itemList.remove(itemList.size() - 1);
            notifyItemRemoved(itemList.size());
        }
    }



}
