package com.thomas.study.basiclayout2.Fragment2Recyclerview;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thomas.study.basiclayout2.R;

/**
 * Created by thomas on 2017-08-06.
 */

public class Tholder extends ViewHolder{

    private TextView frag1content,frag1title;
    private ImageView frag1imageView,frag1rightimageview;
    private Tdata tdata;
    //어댑터에 설정한 인터페이스
    Tadapter.OnItemClickListener mItemClickListener;
    //클릭을 위한 리스너
    public void setOnItemClickListener(Tadapter.OnItemClickListener listener){
        mItemClickListener = listener;
    }


    public Tholder(View itemView) {
        super(itemView);
        frag1title = (TextView) itemView.findViewById(R.id.frag1_title);
        frag1content = (TextView) itemView.findViewById(R.id.frag1_content);
        frag1imageView = (ImageView) itemView.findViewById(R.id.frag1_img);
        frag1rightimageview = (ImageView) itemView.findViewById(R.id.frag1_right_img);

        //클릭시 정보 셋팅
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = getAdapterPosition();
                if(mItemClickListener != null){
                    mItemClickListener.onItemClicked(Tholder.this,view,tdata,position);
                }
            }
        });
    }

    public void setItemData(Tdata data){
        tdata=data;
        frag1title.setText(data.getTitle());
        frag1content.setText(data.getContent());
        frag1imageView.setImageResource(data.getMain_img());
        frag1rightimageview.setImageResource(data.getRight_img());
    }


}
