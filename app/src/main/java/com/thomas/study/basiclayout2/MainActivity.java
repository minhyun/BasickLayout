package com.thomas.study.basiclayout2;

import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity {
        private FragmentTabHost mtabhost;
        private Toolbar mToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mtabhost =(FragmentTabHost)findViewById(android.R.id.tabhost);
        mtabhost.setup(this,getSupportFragmentManager(),R.id.realtabcontent);

        mtabhost.addTab(mtabhost.newTabSpec("1").setIndicator("1번"),Fragment1.class,null);
        mtabhost.addTab(mtabhost.newTabSpec("2").setIndicator("2번"),Fragment2.class,null);
        mtabhost.addTab(mtabhost.newTabSpec("3").setIndicator("3번"),Fragment3.class,null);
        mtabhost.addTab(mtabhost.newTabSpec("4").setIndicator("4번"),Fragment4.class,null);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("LoadMoreRecycleView");
    }
}
