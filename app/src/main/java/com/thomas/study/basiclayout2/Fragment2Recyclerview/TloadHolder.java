package com.thomas.study.basiclayout2.Fragment2Recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.thomas.study.basiclayout2.R;

/**
 * Created by thomas on 2017-08-07.
 */

public class TloadHolder extends RecyclerView.ViewHolder{
    public ProgressBar progressBar;
    public TloadHolder(View itemView) {
        super(itemView);
        progressBar=(ProgressBar)itemView.findViewById(R.id.frag1_progress);
    }
}
