package com.thomas.study.basiclayout2;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.thomas.study.basiclayout2.Fragment2Recyclerview.Tadapter;
import com.thomas.study.basiclayout2.Fragment2Recyclerview.Tdata;


import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment1 extends Fragment implements Tadapter.OnLoadMoreListener,SwipeRefreshLayout.OnRefreshListener{

    private ArrayList<Tdata> itemList ;
    private Tadapter tadapter;
    private SwipeRefreshLayout swipeRefresh;//새로고침
    private ProgressBar progressDialog;
    public Fragment1() {

    }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v =inflater.inflate(R.layout.fragment_fragment1, container, false);
            itemList = new ArrayList<Tdata>();//어레이리스트생성
            swipeRefresh =(SwipeRefreshLayout)v.findViewById(R.id.frag1_swipe);
            progressDialog=(ProgressBar)v.findViewById(R.id.progress);//메인프로그레스
            RecyclerView mRecyclerView = (RecyclerView)v.findViewById(R.id.frag1_RecyclerView);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(mLayoutManager);//어댑터에 LinearLayoutManager 보내기
            mRecyclerView.setVisibility(View.VISIBLE); //리스트 나오기전 로딩이 길때 프로그레스 표시
            progressDialog.setVisibility(View.GONE); //리스트가 나오면 프로그래스 제거
            tadapter = new Tadapter(this);//어댑터 생성
            tadapter.setLinearLayoutManager(mLayoutManager);//LinearLayoutManager 설정
            tadapter.setRecyclerView(mRecyclerView);//어댑터에 RecyclerView 정보 보내기
            tadapter.setOnItemClickListener(new Tadapter.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView.ViewHolder holder, View view, Tdata data, int positiion) {
                    Intent a = new Intent(getActivity(),Activity1.class);
                    startActivity(a);
                }
            });
            mRecyclerView.setAdapter(tadapter); // RecyclerView 어댑터 설정
            swipeRefresh.setOnRefreshListener(this); //SwipeRefresh 리스너

            return v;
        }
    //시작시 데이터 주입
    @Override
    public void onStart() {
        super.onStart();
        loadData();
    }
    //클릭시 할 행동

    //스크롤 위에서 아래로 내렸을때 새로고침
    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefresh.setRefreshing(false);
                loadData();

            }
        },2000);
    }
    //스크롤 아래에서 위로 했을시 더보기
    @Override
    public void onLoadMore() {
        tadapter.setProgressMore(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                itemList.clear();
                tadapter.setProgressMore(false);
                int start = tadapter.getItemCount();
                int end = start + 15;
                for (int i = start + 1; i <= end; i++) {
                    //itemList.add(new Tdata(R.mipmap.ic_launcher,"title"+i,"content"+i, R.mipmap.ic_launcher));
                    Tdata tdata =new Tdata();
                    tdata.setMain_img(R.mipmap.ic_launcher);
                    tdata.setTitle("title"+i);
                    tdata.setContent("content"+i);
                    tdata.setRight_img(R.mipmap.ic_launcher);
                    itemList.add(tdata);
                }
                tadapter.addItemMore(itemList);
                tadapter.setMoreLoading(false);
            }
        },1000);
    }
    //데이터 주입
    private void loadData() {
        itemList.clear();
        for (int i = 1; i <= 20; i++) {
            //itemList.add(new Tdata(R.mipmap.ic_launcher,"title"+i,"content"+i, R.mipmap.ic_launcher));
            Tdata tdata =new Tdata();
            tdata.setMain_img(R.mipmap.ic_launcher);
            tdata.setTitle("title"+i);
            tdata.setContent("content"+i);
            tdata.setRight_img(R.mipmap.ic_launcher);
            itemList.add(tdata);
        }
        tadapter.addAll(itemList);
    }

}





