package com.thomas.study.basiclayout2.Fragment2Recyclerview;

/**
 * Created by thomas on 2017-08-06.
 */

public class Tdata {
    private int main_img;
    private String title;
    private  String content;
    private int right_img;

/*
    public Tdata(int main_img, String title, String content, int right_img) {
        this.main_img = main_img;
        this.title = title;
        this.content = content;
        this.right_img = right_img;
    }*/

    public int getMain_img() {
        return main_img;
    }

    public void setMain_img(int main_img) {
        this.main_img = main_img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getRight_img() {
        return right_img;
    }

    public void setRight_img(int right_img) {
        this.right_img = right_img;
    }
}
